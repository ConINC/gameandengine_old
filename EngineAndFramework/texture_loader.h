#ifndef texture_loader_h_
#define texture_loader_h_

typedef struct tga_hdr{
   char  idlength;
   char  colourmaptype;
   char  datatypecode;
   short int colourmaporigin;
   short int colourmaplength;
   char  colourmapdepth;
   short int x_origin;
   short int y_origin;
   short width;
   short height;
   char  bitsperpixel;
   char  imagedescriptor;
}TGA_Header;

Texture_2D *texture2d_fromfile(char *filename);

Texture_2D *texture2dtga_fromfile(char *filename);
#endif