#ifndef gl_misc_h_
#define gl_misc_h_
#include "stdio.h"
#include "stdlib.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>


///------------------------------------------------------///
/// Contains the attributes of a single Viewport
///------------------------------------------------------///
typedef struct rectangle_struct
{
    int x,y;
    int width, height;
    float ratio;
}Viewport_Rectangle;


///------------------------------------------------------///
/// info-struct for framarate and multi-viewports
///------------------------------------------------------///
typedef struct graphics_info
{
    int window_width, window_height;
    float window_ratio;
    float graphics_clearcolor[4];
    int fixed_framerate;    
    int viewport_count;    
    Viewport_Rectangle player1_viewport;
    Viewport_Rectangle player2_viewport;
    Viewport_Rectangle player3_viewport;
    Viewport_Rectangle player4_viewport;    
    
}GraphicInformation;


///------------------------------------------------------///
///Initialise the graphicsInformation struct for the game to use
///------------------------------------------------------///
GraphicInformation *graphics_viewport_setup(int w, int h, 
                int p_count, float c1,float c2, float c3, 
                float c4, char splitmode);a

///------------------------------------------------------///
///Returns a GLFWwindow instance 
///------------------------------------------------------///
GLFWwindow *graphics_api_initialise(char *game_name, GraphicInformation *graphic_info, int swapinterval);


GLuint _create_vertex_shader(const char* vert_shad_string);
GLuint _create_fragment_shader(const char* frag_shad_string);
GLuint _shader_program_create(const char* vert, const char* frag);



///This function allocates a buffer on the gpu and returns the buffer_id
GLuint create_buffer_float(int size, float *buffer);

GLuint create_buffer_element_int(int size, int *buffer);

#endif