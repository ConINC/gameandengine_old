#ifndef maptypes_h_
#define maptypes_h_

#include "material.h"
#include "vector.h"
#include "collection.h"

typedef struct
{
    unsigned int p_id, uv_id, n_id;
    int          vertex_count;
    Texture_2D   *texture_ptr; //merely a pointer to texture_table entries
    int          is_transparent;
    int          visible;
    
}Map_FaceMesh;

typedef struct
{
    _LIST   *map_faces;
    char    *group_name;
    
}Map_VisualGroup;

typedef struct
{
    VEC3    *points;
    VEC3    *normals;
    int         point_count;
    int         active;
}Map_PhysicalGroup;

typedef struct
{
    int                 id;
    Map_VisualGroup     *visual;
    Map_PhysicalGroup   *physical;
    VEC3            offset_trans;
    VEC3            offset_rot;
}Map_Brush;

typedef struct 
{
    //if name is equal, texture wont be reloaed.
    _LIST *textures_table; //lookup table of textures
    
    char *levelname;
    _LIST *brushes; //Map_Brush
}Level;

#endif