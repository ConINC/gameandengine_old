#include "sprites.h"
#include "material.h"

MenuSprite *create_menuSprite(char *filename, float x, float y, float sx, float sy, float depth)
{    
    MenuSprite* sprite;
    float *points = malloc(sizeof(float) *18);
    float *uvs = malloc(sizeof(float) *12);
    float *normals = malloc(sizeof(float) *18);
    
    
    //x = left/right                z = up/down
    points[0]   = -1*sx + x; points[1]  = -1*sy + y;  points[2] = depth;
    points[3]   = 1*sx + x;  points[4]  = -1*sy + y;  points[5] = depth;
    points[6]   = -1*sx + x; points[7]  = 1*sy + y;  points[8]  = depth;
    points[9]   = -1*sx + x; points[10] = 1*sy + y; points[11]  = depth;
    points[12]  = 1*sx + x; points[13]  = -1*sy + y; points[14] = depth;
    points[15]  = 1*sx + x; points[16]  = 1*sy + y; points[17]  = depth;
    
    normals[0]  = 0; normals[1]     = 0;  normals[2]    = 1;
    normals[3]  = 0; normals[4]     = 0;  normals[5]    = 1;
    normals[6]  = 0; normals[7]     = 0;  normals[8]    = 1;
    normals[9]  = 0; normals[10]    = 0;  normals[11]   = 1;
    normals[12] = 0; normals[13]    = 0;  normals[14]   = 1;
    normals[15] = 0; normals[16]    = 0;  normals[17]   = 1;
    
    uvs[0]  = 0;    uvs[1]     = 0;
    uvs[2]  = 1;    uvs[3]     = 0;
    uvs[4]  = 0;    uvs[5]     = 1;
    uvs[6]  = 0;    uvs[7]     = 1;
    uvs[8]  = 1;    uvs[9]     = 0;
    uvs[10] = 1;    uvs[11]    = 1;
    
    sprite  = malloc(sizeof(MenuSprite));
    sprite->shift_x     = 0;
    sprite->shift_y     = 0;
    sprite->pid         = create_buffer_float(sizeof(float) *18, (float*)points);
    sprite->uvid        = create_buffer_float(sizeof(float) *12,(float*)uvs);
    sprite->nid         = create_buffer_float(sizeof(float) *18, (float*)normals);
    sprite->vert_count  = 6;
    sprite->tex         = texture2dtga_fromfile(filename);
    return sprite;
    
}