/*H**********************************************************************
* FILENAME :        basic_loader.c
*
* DESCRIPTION :
*        Functions to do basic file loading with text/binary.
*
* PUBLIC FUNCTIONS :
*       char        *load_text(const char *filename)
*       int         FM_DecompressFile( FileHandle )
*
* NOTES :
*
* AUTHOR :    Nouri A             START DATE :    30.11.2017
*
* CHANGES :
*
*H*/


#include "basic_loader.h"
#include <stdio.h>

/*********************************************************
 * menu.c                                                *
 *                                                       *
 * Textured overlay w transparency, use as menu button   *
 * or gui works too                                      *
 *                                                       *
 ********************************************************/

/**
 * Load a text file into a char* buffer
 * 
 * @param full path to the file to be read, file extension
 * @return char buffer read from the file
 */

char *load_text(const char *filename)
{
    FILE *file;
    long size;
    char *buf;

    file = fopen(filename, "r");
    if(file == NULL)
        return -1;

    fseek(file, 0L, SEEK_END);
    size = ftell(file);

    fseek(file, 0L, SEEK_SET);

    buf = (char*)calloc(size, sizeof(char));

    if(buf == NULL)
        return 1;

    fread(buf, sizeof(char), size, file);
    fclose(file);
    buf[size-1] = '\0';
    return buf;
}


/**
 * Load a text file into a char* buffer
 * 
 * @param buf_ptr:      char buffer to split
 * @param delimeter:    delimeter character to use 
 * @return Pointer to "Lines" struct
 */

_LIST* split_buffer(char *buf, char *delimeter)
{
    _LIST *list = list_new();

    char *ptr = strtok(buf, delimeter);

    list_add(list, ptr);

    while(ptr != NULL)
    {
        ptr = strtok(NULL, delimeter);
        list_add(list, ptr);
    }
    
    return list;
}

/**
 * Load a text file into a char* buffer
 * 
 * @param buf_ptr:      char buffer to split
 * @param delimeter:    delimeter character to use 
 * @return Pointer to "Lines" struct
 */
char* load_binary(const char* filename)
{
    FILE *file;
    long size;
    char *buffer;
   
    file = fopen(filename, "rb");    
    if(file == NULL)
    {
        printf("Opening failed for: %s", filename);
        return -1;
    }
    else
    {
        printf("Opening succesful for: %s", filename);
    }
    
    fseek(file, 0L, SEEK_END);
    size = ftell(file);

    fseek(file, 0L, SEEK_SET);
    buffer = malloc(sizeof(char)*size);
    
    if(buffer == NULL)
    {    
        printf("Allocating %d byte failed for: %s\n", filename);
        return 1;
    }
    else
    {
        printf("Loaded %s with size %d\n", filename, size);
    }   
    fread(buffer, 1, size*sizeof(char), file);
    
    fclose(file);
         
    return buffer;
}