#ifndef VECTOR4D_HPP
#define VECTOR4D_HPP

#include <iostream>
#include <memory>

class Vector4D
{

private:
    float x,y,z,w;
public:
    Vector4D();
    ~Vector4D();
    Vector4D(float x,float y, float z, float w);
    
    void setx(float x);
    void sety(float y);
    void setz(float z);
    void setw(float w);
    float length(std::shared_ptr<class Vector4D> a);
    std::shared_ptr<class Vector4D> normalize(std::shared_ptr<class Vector4D> in);
}
#endif // VECTOR4D_HPP
