#include "Vector3D.hpp"

Vector3D::Vector3D(float x, float y, float z)  : x(x) ,y (y), z(z){}


Vector3D::~Vector3D()
{

}

std::shared_ptr<class Vector3D> Vector3D::add(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b)
{
    std::shared_ptr<class Vector3D> ptr(new Vector3D(a->x + b->x, a->y + b->y,a->z + b->z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::sub(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b)
{
    std::shared_ptr<class Vector3D> ptr(new Vector3D(a->x - b->x, a->y - b->y,a->z - b->z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::mul(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b)
{
    std::shared_ptr<class Vector3D> ptr(new Vector3D(a->x * b->x,  a->y * b->y,a->z * b->z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::div(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b)
{
    std::shared_ptr<class Vector3D> ptr(new Vector3D(a->x / b->x,  a->y / b->y,a->z / b->z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::div(std::shared_ptr<class Vector3D> v, factor f)
{
    std::shared_ptr<class Vector3D> ptr(new Vector3D(v->x *f,  v->y *f,v->z * f));
    return ptr;
}

float Vector3D::length(std::shared_ptr<class Vector3D> a)
{
    float l = a->x * a->x + a->y * a->y + a->z * a->z;
    return sqrtf(l);
}

std::shared_ptr<class Vector3D> Vector3D::normalize(std::shared_ptr<class Vector3D> in)
{
    float len = length(in);    
    std::shared_ptr<class Vector3D> ptr(new Vector3D(in->x / len,  in->y / len,in->z / len));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::cross(std::shared_ptr<class Vector3D> vec1,std::shared_ptr<class Vector3D> vec2)
{
    float v1 = ((vec1->y * vec2->z) - (vec1->z * vec2->y));
    float v2 = ((vec1->z * vec2->x) - (vec1->x * vec2->z));
    float v3 = ((vec1->x * vec2->y) - (vec1->y * vec2->x));
    
    std::shared_ptr<class Vector3D> ptr(new Vector3D(v1,v2,v3));
    return ptr;
}
float Vector3D::dot(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b)
{
    float v1 = a->x * b->x + a->y * b->y + a->z * b->z;
    return v1;
}

std::shared_ptr<class Vector3D> Vector3D::rotate_x(std::shared_ptr<class Vector3D> a, float f)
{
    float x = a->x              + 0                 + 0;
    float y = 0                 + a->y * cosf(f)    + a->z * -sinf(f);
    float z = 0                 + a->y * sinf(f)    + a->z * cosf(f);
    
    std::shared_ptr<class Vector3D> ptr(new Vector3D(x,y,z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::rotate_y(std::shared_ptr<class Vector3D> a, float f)
{
    float x = a->x * cosf(f)    + 0                 + a->z * sinf(f);
    float y = 0                 + a->y              + 0;
    float z = -a->x * sinf(f)   + 0                 + a->z * cosf(f);
    
    std::shared_ptr<class Vector3D> ptr(new Vector3D(x,y,z));
    return ptr;
}

std::shared_ptr<class Vector3D> Vector3D::rotate_z(std::shared_ptr<class Vector3D> a, float f)
{
    float x = a->x * cosf(f)    + a->y * -sinf(f)   + 0;
    float y = a->x * sinf(f)    + a->y * cosf(f)    + 0;
    float z = 0                 + 0                 + a->z;
    
    std::shared_ptr<class Vector3D> ptr(new Vector3D(x,y,z));
    return ptr;
}

///Return a struct by value, to be compatible with linmath
VEC3 Vector3D::get_values()
{
    VEC3 v;
    v.x = this->x;
    v.y = this->y;
    v.z = this->z;
    
    return v;
}

///Return a struct by value, to be compatible with linmath
void Vector3D::get_values()
{
    printf("Vec: %f %f %f", this->x,this->y,this->z);
}


std::shared_ptr<class Vector3D> Vector3D::rotate(std::shared_ptr<class Vector3D> in, std::shared_ptr<class Vector3D> rot)
{
    std::shared_ptr<class Vector3D> res;
    std::shared_ptr<class Vector3D> xmat;
    std::shared_ptr<class Vector3D> ymat;
    std::shared_ptr<class Vector3D> zmat;
    std::shared_ptr<class Vector3D> ident;
    mat4x4_identity(ident);
    
    xmat = rotate_x(ident,rot->x);
    ymat = rotate_y(ident,rot->y);
    zmat = rotate_z(ident,rot->z);
    
    res =  mul(mul(zmat, mul(ymat, xmat), in);
    
    /*
    mat4x4_mul(m, rotmx,rotmy);
    mat4x4_mul(rotmat, m, rotmz);    
    mat4x4_mul_vec4(res, rotmat, in);
    */
    
    return res;
}

void Vector3D::print()
{
    printf("%.3f %.3f %.3f\n", this->x, this->y, this->z);
}