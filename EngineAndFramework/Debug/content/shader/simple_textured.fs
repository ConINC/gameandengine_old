#version 330 core
in vec2 UV;
in vec3 NORMAL;
out vec4 color;
uniform sampler2D myTextureSampler;
uniform vec2 shift;

void main(){
   vec2 newUV;
   newUV.x = UV.x + shift.x;
   newUV.y = UV.y + shift.y;   
   color = texture( myTextureSampler, newUV.xy ).rgba;
}