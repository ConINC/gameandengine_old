#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNORMAL;
layout(location = 3) in float index;
out vec2 UV;
out vec3 NORMAL;
uniform mat4 VP;
uniform mat4 M;
uniform mat4 Skeleton[3];
uniform mat4 Inverse[3];

void main()
{
	int index_i = int(index);
	
	
	if(index_i == 0)
	{
		gl_Position = VP * M * Skeleton[0] *  Inverse[0] *vec4(vertexPosition_modelspace,1);
		
	}
	if(index_i == 1)	
	{
		mat4 anim2;
		
		anim2 = Skeleton[0];
		anim2 *= Skeleton[1];
		
		mat4 inv2 = Inverse[1];
		mat4 mvp = VP *M;
		
		//gl_Position = mvp * anim2 *inv2 * vec4(vertexPosition_modelspace,1);
		gl_Position = mvp * Skeleton[0] * Skeleton[1] *inv2 * vec4(vertexPosition_modelspace,1);
	}
	if(index_i == 2)	
	{
		mat4 inv3 = Inverse[2];
		mat4 mvp = VP *M;
		
		//gl_Position = mvp * anim2 *inv2 * vec4(vertexPosition_modelspace,1);
		gl_Position = mvp * Skeleton[0] * Skeleton[1]* Skeleton[1] * inv3 * vec4(vertexPosition_modelspace,1);
	}	
	
		
	
	//gl_Position = M * Skeleton[index_i] * vec4(vertexPosition_modelspace,1);
	//gl_Position *= VP;
	//gl_Position += vec4(0,0,0,0);
    UV = vertexUV;
    NORMAL = vertexNORMAL;
};
