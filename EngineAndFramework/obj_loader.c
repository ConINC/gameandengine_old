#include "obj_loader.h"
#include "basic_loader.h"
#include "c_memory.h"


/**
 * Opens a filestream and reads the content into a char* buffer
 *
 * @param const char *filename
 * @return char *buffer
*/

Obj_data_raw *extract_points(_LIST* list)
{
    printf("Extracting points\n");
    int i;
    Obj_data_raw *data  = cmalloc(sizeof(Obj_data_raw));
    data->points        = list_new();
    data->normals       = list_new();
    data->uvs           = list_new();
    data->gfm_lines     = list_new();

    for(i = 0; i < list_count(list); i++)
    {        
        if(strstr(list_at(list, i), "v ") )
        {
            printf("In V");
            char *c = list_at(list, i);
            char *ptr = strtok(c," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            char *ptr4 = strtok(NULL, " ");
            VEC3 *v = cmalloc(sizeof(VEC3));
            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            v->z = (float)atof(ptr4);
            list_add(data->points, v);
            printf("V: %f %f %f\n", v->x,v->y,v->z);
            
        }
        
        if(strstr(list_at(list, i), "vt "))
        {   
            printf("In VT");
            
            char *c = list_at(list, i);
            char *ptr = strtok(c," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            
            VEC2 *v = cmalloc(sizeof(VEC2));
            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            list_add(data->uvs, v);
            printf("VT: %f %f\n", v->x,v->y);
            
        }

        if(strstr(list_at(list, i), "vn ") )
        {
             printf("In VN");
            char *c = list_at(list, i);
            char *ptr = strtok(c," ");
            char *ptr2 = strtok(NULL, " ");
            char *ptr3 = strtok(NULL, " ");
            char *ptr4 = strtok(NULL, " ");
            VEC3 *v = cmalloc(sizeof(VEC3));
            v->x = (float)atof(ptr2);
            v->y = (float)atof(ptr3);
            v->z = (float)atof(ptr4);
            list_add(data->normals, v);
            printf("VN: %f %f %f\n", v->x,v->y,v->z);
        }

        if(strstr(list_at(list, i), "usemtl ") || strstr(list_at(list, i), "mtllib ") || strstr(list_at(list, i), "g ") || strstr(list_at(list, i), "f "))
        {
            list_add(data->gfm_lines, list_at(list, i));
            printf("GMF LINE: %s\n", list_at(list, i));
        }
        printf("STRING: %s\n", list_at(list,i));
    }

    return data;
}

_LIST* create_group_string(Obj_data_raw* dat)
{
    //example of the .obj file
    //g group1
    //usemtl mat1
    //f 1/1/1 2/2/2 4/19/3 3/20/4
    //
    //g group2
    //usemtl mat2
    //f 1/1/1 2/2/2 4/19/3 3/20/4

    //This list contains Lists itself, an Array of Lines for each group!
    _LIST *list_to_grouplists = list_new();
    _LIST *temp_list = NULL;
    int i, x = 0, add_to_list = 0;
    char *mtllib;

    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        //printf("GFMLINE: %s\n", list_at(dat->gfm_lines, i));
    }

    //Remove smoothing groups and get material-library filename
    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        if(strstr(list_at(dat->gfm_lines,i),"mtllib ") != NULL)
        {
            mtllib = list_at(dat->gfm_lines,i);
        }
        if(strstr(list_at(dat->gfm_lines,i),"s ") != NULL)
        {
            list_remove_at(dat->gfm_lines,i);
        }
        if(strstr(list_at(dat->gfm_lines,i),"g default") != NULL)
        {
            list_remove_at(dat->gfm_lines,i);
        }
    }

    //Each group gets its own list now, we need it to build the grouped mesh
    for(i = 0; i < list_count(dat->gfm_lines); i++)
    {
        if(strstr(list_at(dat->gfm_lines,i),"g ") != NULL)
        {
            list_add(list_to_grouplists, temp_list);
            temp_list = list_new();

            if(add_to_list == 1)
                x++;
            add_to_list = 1;
        }

        if(add_to_list == 1 && temp_list != NULL)
        {
            list_add(temp_list,list_at(dat->gfm_lines,i));
            //printf("Adding: %s to list %d\n",list_at(dat->gfm_lines,i), x);
        }

        if(i == list_count(dat->gfm_lines)-1)
            list_add(list_to_grouplists, temp_list);
    }

    return list_to_grouplists;
}

VEC9_INT *extract_face_indices_from_line(char *l)
{
    VEC9_INT *v = cmalloc(sizeof(VEC9_INT));

    char *ptr0,*ptr1,*ptr2,*ptr3,*ptr4,*ptr5,*ptr6,*ptr7,*ptr8,*ptr9;
    ptr0 = strtok(l, " ");

    ptr1 = strtok(NULL, "/");
    ptr2 = strtok(NULL, "/");
    ptr3 = strtok(NULL, " ");

    ptr4 = strtok(NULL, "/");
    ptr5 = strtok(NULL, "/");
    ptr6 = strtok(NULL, " ");

    ptr7 = strtok(NULL, "/");
    ptr8 = strtok(NULL, "/");
    ptr9 = strtok(NULL, " ");

    v->a = atoi(ptr1);
    v->b = atoi(ptr2);
    v->c = atoi(ptr3);

    v->d = atoi(ptr4);
    v->e = atoi(ptr5);
    v->f = atoi(ptr6);

    v->g = atoi(ptr7);
    v->h = atoi(ptr8);
    v->i = atoi(ptr9);

    return v;
}

//Takes a single face via lines
OBJ_Face *make_face(_LIST *face_Lines)
{
    int i;
    char *useless, *matname;
    OBJ_Face *face = cmalloc(sizeof(OBJ_Face));
    face->p_indices = list_new();
    face->n_indices= list_new();
    face->uv_indices= list_new();
    
    for(i = 0; i <list_count(face_Lines);i++)
    {
        if(strstr(list_at(face_Lines, i), "usemtl") != NULL)
        {
            useless = list_at(face_Lines, i);

            useless = strtok(useless, " ");
            matname = strtok(NULL, " ");
            face->material = matname;
        }
        if(strstr(list_at(face_Lines, i), "f ") != NULL)
        {
            VEC9_INT *v = extract_face_indices_from_line(list_at(face_Lines, i));
            
            list_add(face->p_indices, &v->a);
            list_add(face->uv_indices, &v->b);
            list_add(face->n_indices, &v->c);
            
            list_add(face->p_indices, &v->d);
            list_add(face->uv_indices, &v->e);
            list_add(face->n_indices, &v->f);
            
            list_add(face->p_indices, &v->g);
            list_add(face->uv_indices, &v->h);
            list_add(face->n_indices, &v->i);
        }
    }
    return face;
}

//take a group via lines
OBJ_Group *make_single_group(_LIST *l)
{
    int i,j;
    char *group;
    OBJ_Group *g = cmalloc(sizeof(OBJ_Group));
    g->faces = list_new();
    _LIST *list_of_faces = list_new();
    _LIST *face_temp;
    int startReading = 0;
    int counter = 0;

    for(i = 0; i < list_count(l); i++)
    {
        if(strstr(list_at(l,i),"g ") != NULL)
        {
            group = list_at(l,i);
            list_remove_at(l,i);
        }
    }

    for(i = 0; i < list_count(l); i++)
    {
        if( strstr(list_at(l,i),"usemtl ") != NULL)
        {
            printf("\n\n");
            counter++;
            list_add(list_of_faces, face_temp);

            face_temp = list_new();
        }
        //printf("Line %s to face %d\n",list_at(l,i), counter);
        list_add(face_temp,list_at(l,i));
    }
    list_remove_at(list_of_faces,0);
    list_add(list_of_faces, face_temp);

    //printf("\n\n");

    //CHecking if faces were made
    for(i = 0; i < list_count(list_of_faces); i++)
    {
        _LIST *temp_check = list_at(list_of_faces, i);
        int cnt = list_count(temp_check);
        //printf("\n\nCount of Face %d is %d\n", i, cnt);

        //MAKE THE FACE HERE!
        OBJ_Face *f = make_face(temp_check);
        list_add(g->faces, f);

        for(j = 0; j < list_count(temp_check); j++)
        {
            //printf("%s\n", list_at(temp_check,j));
        }
    }
    g->groupname = group;
    return g;
}

void print_group(OBJ_Group *group)
{
    printf("Printing Group: %s\n", group->groupname);
    int i,j;
    int pcount, uvcount, ncount;
    
    int facecount = list_count(group->faces);
    for(i = 0; i < facecount; i++)
    {
        OBJ_Face *obj_face = list_at(group->faces, i);
        printf("Face:\nMaterial: %s\n", obj_face->material);
        
        pcount = list_count(obj_face->p_indices);

        for(j = 0; j < pcount; j++)
        {
            int *d = list_at(obj_face->p_indices,j);
            printf("%d\n", *d );
        }
        
        ncount = list_count(obj_face->uv_indices);
        for(j = 0; j < list_count(obj_face->uv_indices); j++)
        {
            int *d = list_at(obj_face->uv_indices,j);
            printf("%d\n", *d);
        }
        
        uvcount = list_count(obj_face->n_indices);
        for(j = 0; j < list_count(obj_face->n_indices); j++)
        {
            int *d = list_at(obj_face->n_indices,j);
            printf("%d\n", *d);
        }
    }
    
}

_LIST* make_groups(_LIST *group_lines)
{
    int i,j;
    _LIST *groups = list_new();

    for(i = 0; i < list_count(group_lines); i++)
    {
        _LIST *l_temp = list_at(group_lines,i);
        OBJ_Group *g = make_single_group(l_temp);
        print_group(g);
        list_add(groups, g);
    }
    return groups;
}

void print_list_as_float3(_LIST *l)
{
    int i;
    for(i = 0; i < list_count(l); i++)
    {
        VEC3 *v;
        v = list_at(l, i);
        printf("%f %f %f\n", v->x, v->y,v->z);
    }
}

void print_list_as_float2(_LIST *l)
{
    int i;
    for(i = 0; i < list_count(l); i++)
    {
        VEC2 *v;
        v = list_at(l, i);
        printf("%d: %f %f\n", i, v->x, v->y);
    }
}

void print_obj_model(OBJ_MODEL *model)
{
    printf("PRINTING MODEL:\n");
    print_list_as_float3(model->p);
    print_list_as_float2(model->uv);
    print_list_as_float3(model->n);

    int i;
    for(i = 0; i < list_count(model->groups); i++)
    {
        OBJ_Group *g = list_at(model->groups, i);
        printf("Groupname: %s\n", g->groupname);
        int j;
        for(j = 0; j < list_count(g->faces); j++)
        {
            OBJ_Face *f = list_at(g->faces,j);
            printf("Mat: %s\n",f->material);
            int k;

            for(k = 0; k < list_count(f->p_indices); k+=3)
            {
                int *d1 = list_at(f->p_indices, k);
                int *d2 = list_at(f->p_indices, k+1);
                int *d3 = list_at(f->p_indices, k+2);
                printf("P I: %d %d %d\n", *d1, *d2, *d3);
            }
            for(k = 0; k < list_count(f->uv_indices); k+=2)
            {
                int *d1 = list_at(f->uv_indices, k);
                int *d2 = list_at(f->uv_indices, k+1);
                printf("UV I: %d %d\n", *d1, *d2);
            }
            for(k = 0; k < list_count(f->n_indices); k+= 3)
            {
                int *d1 = list_at(f->n_indices, k);
                int *d2 = list_at(f->n_indices, k+1);
                int *d3 = list_at(f->n_indices, k+2);
                printf("N I: %d %d %d\n", *d1, *d2, *d3);
            }
        }
    }
}

_LIST *load_materials(char *fileandpath)
{
    int i,j, counter;
    char *buf                   = load_text(fileandpath);
    _LIST *lines                = split_buffer(buf, "\n");
    _LIST *list_of_materials    = list_new();
    _LIST *mat_lines_temp       =  list_new();
    _LIST *obj_material_list    = list_new();
    
    
    //Seperate the lines into multiple sets of lines
    //Short: each material goes to its own list
    for(i = 0; i < list_count(lines); i++)
    {
        if( strstr(list_at(lines,i),"newmtl ") != NULL)
        {
            printf("\n\n");
            counter++;
            list_add(list_of_materials, mat_lines_temp);

            mat_lines_temp = list_new();
        }
        printf("Line: %s to Face: %d\n",list_at(lines,i), counter);
        if( list_at(lines,i) != NULL )
        {
            list_add(mat_lines_temp,list_at(lines,i));
        }
    }
    list_remove_at(list_of_materials,0);
    list_add(list_of_materials, mat_lines_temp);
    
    
    for(i = 0; i < list_count(list_of_materials); i++)
    {
        OBJ_MATERIAL *mat = cmalloc(sizeof(OBJ_MATERIAL));
        mat->colormap_path = NULL;
        mat->specularmap_path = NULL;
        mat->normalmap_path = NULL;
        
        _LIST *single_mat_lines = list_at(list_of_materials, i);        
        for(j = 0; j < list_count(single_mat_lines); j++)
        {
            if(strstr(list_at(single_mat_lines, j), "newmtl"))
            {
                char *c =  cmalloc(strlen(list_at(single_mat_lines, j)));
                strcpy(c, list_at(single_mat_lines, j));
                char *ptr = strtok(c," ");
                char *ptr2 = strtok(NULL, " ");
                mat->name = ptr2;
            }
            if(strstr(list_at(single_mat_lines, j), "map_Kd"))
            {
                char *c =  cmalloc(strlen(list_at(single_mat_lines, j)));
                strcpy(c, list_at(single_mat_lines, j));
                char *ptr = strtok(c," ");
                char *ptr2 = strtok(NULL, " ");
                mat->colormap_path = ptr2;
            }
            
            if(strstr(list_at(single_mat_lines, j), "bump"))
            {
                char *c =  cmalloc(strlen(list_at(single_mat_lines, j)));
                strcpy(c, list_at(single_mat_lines, j));
                char *ptr = strtok(c," ");
                char *ptr2 = strtok(NULL, " ");
                mat->normalmap_path = ptr2;
            
            }
            
            if(strstr(list_at(single_mat_lines, j), "map_Ks"))
            {
                char *c =  cmalloc(strlen(list_at(single_mat_lines, j)));
                strcpy(c, list_at(single_mat_lines, j));
                char *ptr = strtok(c," ");
                char *ptr2 = strtok(NULL, " ");
                mat->specularmap_path = ptr2;
            
            }
        }
        
        
        printf("Matname: %s\n", mat->name);
        if(mat->colormap_path != NULL) printf("Color: %s\n",mat->colormap_path);
        if(mat->normalmap_path != NULL) printf("Normal: %s\n", mat->normalmap_path);
        if(mat->specularmap_path != NULL) printf("Specular: %s\n",mat->specularmap_path);
        getchar();
        list_add(obj_material_list, mat);
        
    }

    return obj_material_list;
}


OBJ_MODEL *load_obj_model(char *root, char *obj_mtl_name)
{
    char *obj_filename_and_path     = cmalloc(256);
    char *mtl_filename_and_path     = cmalloc(256);
    
    strcpy(obj_filename_and_path, root);
    strcat(obj_filename_and_path, obj_mtl_name);
    strcat(obj_filename_and_path, ".obj");
    
    strcpy(mtl_filename_and_path, root);
    strcat(mtl_filename_and_path, obj_mtl_name);
    strcat(mtl_filename_and_path, ".mtl");
    
    char *t = load_text(obj_filename_and_path);
    _LIST *l = split_buffer(t, "\n");
    int i;
    
    
    for(i = 0; i < list_count(l); i++)
        printf("%s\n", list_at(l, i));
    

    Obj_data_raw *dat = extract_points(l);
    
    
    
    
    _LIST *group_lines = create_group_string(dat);
    _LIST *groups_done = make_groups(group_lines);  
    
    OBJ_MODEL *m = cmalloc(sizeof(OBJ_MODEL));
    m->groups = groups_done;
    m->n = dat->normals;
    m->p = dat->points;
    m->uv = dat->uvs;    
    m->materials = load_materials(mtl_filename_and_path);
    print_obj_model(m);
    return m;
}