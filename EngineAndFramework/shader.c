#include "shader.h"
#include "stdlib.h"

///------------------------------------------------------///
///Creates and returns a shader object, linking 
///and compilink the vs and fs from file with filename.vs/fs
///------------------------------------------------------///
SHADER_TEXTURE_ONLY* create_texture_only_shader(char *program_name, char *file_vs, char *file_ps)
{
    //fix static alloc, wtf?
    /*
    char *vs_path = malloc(256*sizeof(char)); vs_path[0] = '\0';
    char *fs_path = malloc(256*sizeof(char)); fs_path[0] = '\0';
    strcat(vs_path, file_name); strcat(vs_path, ".vs");
    strcat(fs_path, file_name); strcat(fs_path, ".fs");
    */
    SHADER_TEXTURE_ONLY *shader;
    
    char *vs            = load_text(file_vs);
    char *fs            = load_text(file_ps);
    shader              =     malloc(sizeof(SHADER_TEXTURE_ONLY));
    shader->programID   = _shader_program_create(vs,fs);
    shader->programName = program_name;
    shader->ps_text     = fs;
    shader->vs_text     = vs;
    
    shader->samplerUniformLocation  = glGetUniformLocation(shader->programID, "myTextureSampler");
    shader->matrixUniformLocation   = glGetUniformLocation(shader->programID, "MVP");
    shader->shiftUvUniformLocation  = glGetUniformLocation(shader->programID, "shift");
    
    return shader;
}

///------------------------------------------------------///
///Creates and returns a shader object, linking 
///and compilink the vs and fs from file with filename.vs/fs
///------------------------------------------------------///
SHADER_SKINNING_TEST_ONLY* create_skinning_test_only_shader(char *program_name, char *file_vs, char *file_ps)
{
    SHADER_SKINNING_TEST_ONLY *shader;
    char *vs    = load_text(file_vs);
    char *fs    = load_text(file_ps);
   
    shader              = malloc(sizeof(SHADER_SKINNING_TEST_ONLY));
    shader->programID   = _shader_program_create(vs,fs);
    shader->programName = program_name;
    shader->ps_text     = fs;
    shader->vs_text     = vs;
    
    shader->samplerUniformLocation      = glGetUniformLocation(shader->programID, "myTextureSampler");
    shader->vpUniformLocation           = glGetUniformLocation(shader->programID, "VP");
    shader->mUniformLocation            = glGetUniformLocation(shader->programID, "M");
    shader->skeletonUniformLocation     = glGetUniformLocation(shader->programID, "Skeleton");
    shader->inverseBindUniformLocation  = glGetUniformLocation(shader->programID, "Inverse");
    return shader;
}

//remove filename, since shaders are fixed ?
SHADER_TEXTURE_DIFFUSE_AMBIENT* create_texture_diffuse_ambient_shader(char* program_name, char *file_name)
{
    SHADER_TEXTURE_DIFFUSE_AMBIENT *shader;
    char *vs_path   = malloc(512*sizeof(char)); vs_path[0] = '\0';
    char *fs_path   = malloc(512*sizeof(char)); fs_path[0] = '\0';
    strcat(vs_path, file_name); strcat(vs_path, ".vs");
    strcat(fs_path, file_name); strcat(fs_path, ".fs");
    
    char *vs            = load_text(vs_path);
    char *fs            = load_text(fs_path);
    shader              = malloc(sizeof(SHADER_TEXTURE_DIFFUSE_AMBIENT));
    shader->programID   = _shader_program_create(vs,fs);
    shader->programName = program_name;
    shader->ps_text     = fs;
    shader->vs_text     = vs;
    shader->samplerUniformLocation      = glGetUniformLocation(shader->programID, "DiffuseSampler");
    shader->matrixUniformLocation       = glGetUniformLocation(shader->programID, "MVP");
    shader->ambientUniformLocation      = glGetUniformLocation(shader->programID, "AMBIENT");
    shader->diffuse_color_uniform       = glGetUniformLocation(shader->programID, "DIFFUSE_COL");
    shader->diffuse_direction_uniform   = glGetUniformLocation(shader->programID, "DIFFUSE_DIR");
    
    return shader;
}

GLuint _create_vertex_shader(const char* vert_shad_string)
{
    GLuint vertex_shader;
	vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vert_shad_string, NULL);
    glCompileShader(vertex_shader);
    
    int status;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);
    if(status == GL_TRUE)
        puts("Compiled vertex good");
    else
        puts("Failed compile vertex");
            
    return vertex_shader;
}

GLuint _create_fragment_shader(const char* frag_shad_string)
{
	GLuint fragment_shader;
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &frag_shad_string, NULL);
    glCompileShader(fragment_shader);
    
    int status;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &status);
    if(status == GL_TRUE)
        puts("Compiled fragment good");
    else
        puts("Failed compile fragment");
        
    return fragment_shader;
}

GLuint _shader_program_create(const char* vert, const char* frag)
{
	GLuint program = glCreateProgram();
    glAttachShader(program, _create_vertex_shader(vert));
    glAttachShader(program, _create_fragment_shader(frag));
    glLinkProgram(program);
    
    int status;
    glGetProgramiv(program, GL_LINK_STATUS, &status);
       
    if(status == GL_TRUE)
        printf("Link program succesful\n");
    else
        puts("Failed to link\n");
    
    return program;
}
