#include "draw.h"
#include "sprites.h"

///------------------------------------------------------///
///Draw a non-animated 3d model with points, uv, normal and single-texture
///------------------------------------------------------///

void draw_static_model_texture_only(float *vp_mat, float *m_mat, Model *model_ptr, SHADER_TEXTURE_ONLY *shader_ptr)
{       
    mat4x4 a;
    mat4x4 model_mat; 
    mat4x4 cam_mat;
    
    mat4x4_dup(&model_mat, m_mat);
    mat4x4_dup(&cam_mat, vp_mat);
    mat4x4_mul(&a, &cam_mat, &model_mat);
    
    glUseProgram(shader_ptr->programID);
    
    
	glUniformMatrix4fv(shader_ptr->matrixUniformLocation, 1, GL_FALSE, &a);
    glUniform1i(shader_ptr->samplerUniformLocation, 0); //Use sampler slot 0   
    //glUniform4f(shader_ptr->ambientUniformLocation, 0.5,0.5,0.5,1);    
    //glUniform4f(shader_ptr->diffuse_color_uniform, 0.5,0.5,0.5,1);    
    //glUniform3f(shader_ptr->diffuse_direction_uniform, 1,0,0);
    //glUniform3f(shader_ptr->diffuse_direction_uniform, -camera->forward_direction[0],-camera->forward_direction[1],-camera->forward_direction[2]);
    
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model_ptr->texture_ptr->texture_id);
	
    // 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,model_ptr->p_id);
	glVertexAttribPointer(
        0,                  // attribute.  must match the layout in the shader.
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
        );
        
	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, model_ptr->uv_id);
	glVertexAttribPointer(
        1,                                // attribute. must match the layout in the shader.
        2,                                // size : U+V => 2
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
        
    // 3nd attribute buffer : Normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, model_ptr->n_id);
	glVertexAttribPointer(
        2,                                // attribute.  must match the layout in the shader.
        3,                                // size : U+V => 2
        GL_FLOAT,                         // type
        GL_TRUE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, model_ptr->vertex_count); // 12*3 indices starting at 0 -> 12 triangles
    
    //free(result);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}


float uvuv =0;

///------------------------------------------------------///
///Draw a non-animated 3d model with points, uv, normal and single-texture
///------------------------------------------------------///
void draw_gui_texture_only(MenuSprite *sprite, float x, float y, float sx, float sy,SHADER_TEXTURE_ONLY *shader_ptr)
{
    mat4x4 a;
    mat4x4_identity(a);
    
    glUseProgram(shader_ptr->programID);
    glUniform2f(shader_ptr->shiftUvUniformLocation, sprite->shift_x, sprite->shift_y);    
	glUniformMatrix4fv(shader_ptr->matrixUniformLocation, 1, GL_FALSE, &a);
    glUniform1i(shader_ptr->samplerUniformLocation, 0); //Use sampler slot 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, sprite->tex->texture_id);
	
    // 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,sprite->pid);
	glVertexAttribPointer(
        0,                  // attribute.  must match the layout in the shader.
        3,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
        );
        
	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, sprite->uvid);
	glVertexAttribPointer(
        1,                                // attribute. must match the layout in the shader.
        2,                                // size : U+V => 2
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
        
    // 3nd attribute buffer : Normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, sprite->nid);
	glVertexAttribPointer(
        2,                                // attribute.  must match the layout in the shader.
        3,                                // size : U+V => 2
        GL_FLOAT,                         // type
        GL_TRUE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
        
	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, sprite->vert_count); // 12*3 indices starting at 0 -> 12 triangles
    
    //free(result);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
  
}


///------------------------------------------------------///
///Draw a non-animated 3d model with points, uv, normal and single-texture
///------------------------------------------------------///
float rot = 0, rot2 = 0;
void draw_animated_model_test_only(MouseKeyboard_Input* input, Joint_Key **keys, float anim_time, float *vp_mat, float *m_mat, Model *model_ptr, SHADER_SKINNING_TEST_ONLY *shader_ptr)
{
    mat4x4 model_mat; 
    mat4x4 cam_mat;
    mat4x4 bone2_temp;
    mat4x4 ident;
    mat4x4_identity(&ident);
    mat4x4_translate(model_mat, 0,3,0);
    
    //to test skinning
    if(input->mouse_l == 1)
    {
        rot += 0.015f;
    }
    if(input->mouse_r == 1)
    {
        rot -= 0.015f;
    }
    
    if(input->q == 1)
    {
        rot2 += 0.015f;
    }
    if(input->e == 1)
    {
        rot2 -= 0.015f;
    }
    
    Vector3D t1_bone = {0,0,-0};
    Vector3D t2_bone = {0,0,-6};
    Vector3D t3_bone = {0,0,-12};
    Vector3D r1_bone = {rot,0,0};
    Vector3D r2_bone = {rot2,0,0};
    Vector3D r3_bone = {rot2,0,0};
    
    
    Vector3D t1_inv = {0,0,-0};
    Vector3D t2_inv = {0,0,-6};    
    Vector3D t3_inv = {0,0,-12};    
       
    Vector3D r1_inv = {0,0,0};
    Vector3D r2_inv = {0,0,0};
    Vector3D r3_inv = {0,0,0};
    
    mat4x4 bones_initial[3];
    mat4x4 bones_final[3]; // bone2 = bone2 ;  bone1 = bone1;
    mat4x4 inv_pose[3]; 
    mat4x4 pose[3];
    
    mat4x4 bones_final_temp[3];
    
    mat4x4_world(&pose[0], t1_inv,r1_inv,1);
    mat4x4_world(&pose[1], t2_inv,r2_inv,1);  
    mat4x4_world(&pose[2], t2_inv,r2_inv,1);  
    mat4x4_invert(&inv_pose[0],&pose[0]);
    mat4x4_invert(&inv_pose[1],&pose[1]);
    mat4x4_invert(&inv_pose[2],&pose[2]);
    
    mat4x4_world(&bones_initial[0], t1_bone,r1_bone,1);
    mat4x4_world(&bones_initial[1], t2_bone,r2_bone,1);
    mat4x4_world(&bones_initial[2], t3_bone,r3_bone,2);
    
    
    mat4x4 bone2_final_temp;
    //mat4x4_mul(bone2_final_temp, bones_final[1], bones_final[0]);
    //mat4x4_dup(bones_final[1], bone2_final_temp);
    
    //mat4x4_mul(bones_final[0], bones_initial[0], inv_pose[0]);
    //mat4x4_mul(bones_final[1],  bones_initial[1],inv_pose[1]);
    
    glUseProgram(shader_ptr->programID);
    
	glUniformMatrix4fv(shader_ptr->vpUniformLocation, 1, GL_FALSE, vp_mat);
    glUniformMatrix4fv(shader_ptr->mUniformLocation, 1, GL_FALSE, &model_mat);
    glUniform1i(shader_ptr->samplerUniformLocation, 0); //Use sampler slot 0
    glUniformMatrix4fv(shader_ptr->skeletonUniformLocation, 3, GL_FALSE, &bones_initial); //Use sampler slot 1
    glUniformMatrix4fv(shader_ptr->inverseBindUniformLocation, 3, GL_FALSE, &inv_pose); //Use sampler slot 1
      
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, model_ptr->texture_ptr->texture_id);
	
    // 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER,model_ptr->p_id);
	glVertexAttribPointer(
        0,                  // attribute.  must match the layout in the shader.
        3,                  // size X Y Z => 3
        GL_FLOAT,           // type
        GL_FALSE,           // normalized?
        0,                  // stride
        (void*)0            // array buffer offset
        );
        
	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, model_ptr->uv_id);
	glVertexAttribPointer(
        1,                                // attribute. must match the layout in the shader.
        2,                                // size : U+V => 2
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
        
    // 3nd attribute buffer : Normals
	glEnableVertexAttribArray(2);
	glBindBuffer(GL_ARRAY_BUFFER, model_ptr->n_id);
	glVertexAttribPointer(
        2,                                // attribute.  must match the layout in the shader.
        3,                                // size : X Y Z => 3
        GL_FLOAT,                         // type
        GL_FALSE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
    
    glEnableVertexAttribArray(3);
	glBindBuffer(GL_ARRAY_BUFFER, model_ptr->w_id);
	glVertexAttribPointer(
        3,                                // attribute.  must match the layout in the shader.
        1,                                // size : Single Index for now => 2
        GL_FLOAT,                          // type
        GL_FALSE,                         // normalized?
        0,                             // stride
        (void*)0       // array buffer offset
        );
        
	// Draw the triangle !
	glDrawArrays(GL_TRIANGLES, 0, model_ptr->vertex_count); // 12*3 indices starting at 0 -> 12 triangles
    
    //free(result);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
    glDisableVertexAttribArray(3);
}