#ifndef input_h_
#define input_h_

#include <stdlib.h>
#include <stdio.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

typedef struct mouse_keyboard_input
{
    int w, a, s, d, e, q, f, g,t, lshift, rshift, space,lctrl, rctrl;
    int mouse_l, mouse_r;
    float mouse_y, mouse_x;
    
}MouseKeyboard_Input;

void update_mouse_kb_input(MouseKeyboard_Input *input);
MouseKeyboard_Input kb_input_init();

#endif