#include <stdlib.h>
#include "game.h"


int main(int argc, char **argv)
{      
    game_load();
    printf("Game loaded");
    
    while (!glfwWindowShouldClose(game.main_window))
    {
        game_update();
        game_draw();
    }
    
    glfwDestroyWindow(game.main_window);
    glfwTerminate();
    
    exit(EXIT_SUCCESS);
    
}