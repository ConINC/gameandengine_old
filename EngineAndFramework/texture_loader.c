#include "material.h"

Texture_2D *texture2d_fromfile(char *filename)
{
    FILE * file;
    Texture_2D *tex;
    char *buffer;
    
    //Load the bitmap
    file = fopen(filename,"rb");
    tex = malloc(sizeof(Texture_2D));
    
    fseek(file,18, SEEK_SET);
    fread(&tex->w,1,4,file);
    fread(&tex->h,1,4,file);
    fseek(file,54, SEEK_SET);    
    
    buffer = malloc(3* tex->w * tex->h);
    fread(buffer, 1, tex->w * tex->h * 3, file);
    fclose(file);    

   
    tex->filename = filename;
        
    free(buffer);    
        
    printf("\n\nTexture Info:\n%s\nw = %d h = %d\n", filename, tex->w, tex->h);
    return tex;
}

Texture_2D *texture2dtga_fromfile(char *filename)
{
    FILE * file;
    Texture_2D *tex;
    char *buffer;
    int i;
    short w,h;
    //Load the bitmap
    file = fopen(filename,"rb");
    tex = malloc(sizeof(Texture_2D));
    
    fseek(file,12, SEEK_SET);
    fread(&w,1,2,file);
    fread(&h,1,2,file);
    fseek(file,18, SEEK_SET);
    tex->w = w; 
    tex->h = h;
    buffer = malloc(4* tex->w * tex->h);
    fread(buffer, 1, tex->w * tex->h * 4, file);
    fclose(file);
    
    for(i = 0; i < 4* tex->w * tex->h; i+=4)
    {
        //printf("%d ", buffer[i+3]);
        
    }
    
    //Binding the texture with openGL
    glActiveTexture(GL_TEXTURE0); //Bind it all into slot 0 for now
    glGenTextures(1, &(tex->texture_id));
    glBindTexture(GL_TEXTURE_2D, tex->texture_id);
    
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);    
    glTexImage2D(GL_TEXTURE_2D, 0,GL_RGBA8, tex->w, tex->h, 0, GL_BGRA, GL_UNSIGNED_BYTE, buffer);
    
    tex->filename = filename;
        
    free(buffer);
    
    printf("\n\nTexture Info:\n%s\nw = %d h = %d\n", filename, tex->w, tex->h);
    return tex;
}
