#ifndef material_h_
#define material_h_
#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>

//Texture 2D
typedef struct texture2d_str
{   
    char *name;
    char *filename, filename_n, filename_spec;
    char bitsperpixel;
    unsigned int texture_id, normal_id, spec_id;
    int w, h, w_n, h_n, w_s, h_s;
}Texture_2D;


#endif