#include "gl_testing_functions.h"

int gl_func_enable_wireframe_switch_timer = 0;
int gl_func_enable_wireframe_switch_state = 1;

void gl_func_enable_wireframe(MouseKeyboard_Input *i)
{
    if(i->rctrl == 1)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    if(i->rshift == 1)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
}