#ifndef game_h_
#define game_h_

#include <stdio.h>
#include <stdlib.h>
#include "gl_misc.h"
#include "gl_layer.h"
#include "input.h"
#include "camera.h"
#include "model.h"
#include "shader.h"
#include "animation.h"
#include "sprites.h"
#include "menu.h"
#include "maptypes.h"

typedef struct game_str
{
    GraphicInformation *graph_info;        
    GLFWwindow *main_window;
    MouseKeyboard_Input *input;
    Camera *cam;
    Model *model,*m2; 
    SHADER_TEXTURE_ONLY *shader;
    GameMenu_1 *menu;
    SHADER_SKINNING_TEST_ONLY *shader_skin;
    Model *animated;
    Model *indexed_model;
    float v;
    Level *map;
        
}Game;

extern Game game;

void game_load();
void game_update();
void game_draw();

#endif