#ifndef shader_h_
#define shader_h_

#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>


typedef struct shader_texture_only_str
{    
    unsigned int programID;
    unsigned int samplerUniformLocation;
    unsigned int matrixUniformLocation;
    unsigned int shiftUvUniformLocation;
    
    char *programName;
    char *vs_text;
    char *ps_text;
    
}SHADER_TEXTURE_ONLY;

typedef struct shader_skinning_test_only_str
{    
    unsigned int programID;
    unsigned int samplerUniformLocation;
    unsigned int mUniformLocation;
    unsigned int vpUniformLocation;
    unsigned int skeletonUniformLocation;
    unsigned int inverseBindUniformLocation;
    char *programName;
    char *vs_text;
    char *ps_text;
}SHADER_SKINNING_TEST_ONLY;

typedef struct shader_texture_diffuse_ambient_str
{   
    unsigned int programID;
    unsigned int samplerUniformLocation;
    unsigned int matrixUniformLocation;
    unsigned int ambientUniformLocation;
    unsigned int diffuse_color_uniform;
    unsigned int diffuse_direction_uniform;
    char *programName;
    char *vs_text;
    char *ps_text;
}SHADER_TEXTURE_DIFFUSE_AMBIENT;

SHADER_TEXTURE_ONLY* create_texture_only_shader(char *program_name, char *file_vs, char *file_ps);
SHADER_TEXTURE_DIFFUSE_AMBIENT *create_texture_diffuse_ambient_shader(char* program_name, char *file_name);
SHADER_SKINNING_TEST_ONLY* create_skinning_test_only_shader(char *program_name, char *file_vs, char *file_ps);

///Dont use directly!
unsigned int _create_vertex_shader(const char* vert_shad_string);
unsigned int _create_fragment_shader(const char* frag_shad_string);
unsigned int _shader_program_create(const char* vert, const char* frag);


#endif