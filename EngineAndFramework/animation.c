#include "animation.h"
#include "model.h"

void joint_update(Joint *j, Vector3D t, Vector3D r, float s)
{
    j->r = r;
    j->t = t;
    j->s = s;
    mat4x4 gident,m2;
    mat4x4 rotmatrix,rotmatrix_x,rotmatrix_y,rotmatrix_z;
    mat4x4 translate;
    mat4x4 scale;
    mat4x4 final_mat;
    
    mat4x4_identity(&gident);
    mat4x4_scale(&scale, &gident, s);
    
    mat4x4_rotate_X(&rotmatrix_x,&gident,r.x);
    mat4x4_rotate_Y(&rotmatrix_y,&gident,r.y);
    mat4x4_rotate_Z(&rotmatrix_z,&gident,r.z);
    mat4x4_mul(&m2, &rotmatrix_x, &rotmatrix_y);
    mat4x4_mul(&rotmatrix, &m2, &rotmatrix_z);    
    
    mat4x4_translate(&translate,t.x,t.y,t.z);
    
    mat4x4_mul(&final_mat, &rotmatrix, &gident);
    mat4x4_mul(&m2, &translate, &final_mat);
    mat4x4_dup(j->matrix, &m2);
}

void mat4x4_world(mat4x4 res, Vector3D t, Vector3D r, float s)
{
    mat4x4 gident,m2;
    mat4x4 rotmatrix,rotmatrix_x,rotmatrix_y,rotmatrix_z;
    mat4x4 translate;
    mat4x4 scale;
    mat4x4 final_mat;
    
    mat4x4_identity(&gident);
    mat4x4_scale(&scale, &gident, s);
    
    mat4x4_rotate_X(&rotmatrix_x,&gident,r.x);
    mat4x4_rotate_Y(&rotmatrix_y,&gident,r.y);
    mat4x4_rotate_Z(&rotmatrix_z,&gident,r.z);
    mat4x4_mul(&m2, &rotmatrix_x, &rotmatrix_y);
    mat4x4_mul(&rotmatrix, &m2, &rotmatrix_z);    
    
    mat4x4_translate(&translate,t.x,t.y,t.z);
    
    mat4x4_mul(&final_mat, &rotmatrix, &gident);
    mat4x4_mul(&m2, &translate, &final_mat);
    mat4x4_dup(res, &m2);
    int penis;
}
