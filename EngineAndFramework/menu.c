#include "menu.h"
#include "game.h"

GameMenu_1 *start_menu_init()
{
    GameMenu_1 *m   = malloc(sizeof(GameMenu_1));
    
}

int is_mouse_hover(MenuButton *b, float mx,float my, int screen_x, int screen_y)
{
    
}

MenuButton *create_menu_button(char *filename, float x, float y, float sx, float sy, float depth)
{
    MenuButton *b   = malloc(sizeof(MenuButton));
    b->sprite       =  create_menuSprite(filename, x,y,sx,sy,depth);
    
    return b;
}

GameMenu_1 *create_game_menu_1()
{
    GameMenu_1 *menu    = malloc(sizeof(GameMenu_1));
    
    menu->bg_title      = create_menu_button("content/models/bg_1080_title.tga", 0,0, 1,1, 0.5);
    menu->bg_wire       = create_menu_button("content/models/bg_1080_wireframe.tga", 0,0, 1,1, 0.4);    
    menu->m1_c          = create_menu_button("content/models/menu_rect_cp.tga", -0.51, 0.0, 0.25,0.4, -1);
    menu->m1_e          = create_menu_button("content/models/menu_rect_editor.tga", 0.0, 0.0, 0.25,0.4, -1);
    menu->m1_m          = create_menu_button("content/models/menu_rect_mult.tga", 0.51,0.0, 0.25,0.4, -1);
    menu->m1_s          = create_menu_button("content/models/gear_icon.tga", 0.93,0.90,0.05*0.5,0.08*0.5,-1);
    menu->m1_q          = create_menu_button("content/models/quit_icon.tga", -0.93,-0.90,0.05*0.5,0.08*0.5,-1);
    
    return menu;
}

void draw_game_menu(GameMenu_1* menu, Game *game)
{
    draw_gui_texture_only(game->menu->bg_title->sprite, 0,0,1,1, game->shader);   
    draw_gui_texture_only(game->menu->bg_wire->sprite, 0,0,1,1, game->shader);             
    draw_gui_texture_only(game->menu->m1_c->sprite, 0,0,1,1,game->shader);        
    draw_gui_texture_only(game->menu->m1_m->sprite, 0,0,1,1,game->shader);        
    draw_gui_texture_only(game->menu->m1_e->sprite, 0,0,1,1,game->shader);    
    draw_gui_texture_only(game->menu->m1_s->sprite, 0,0,1,1,game->shader);    
    draw_gui_texture_only(game->menu->m1_q->sprite , 0,0,1,1,game->shader);     
} 


