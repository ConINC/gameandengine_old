#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNORMAL;
layout(location = 3) in float joint_index;

out vec2 UV;
out vec3 NORMAL;
uniform mat4 MVP;
uniform mat4 skeleton[2];

void main(){   
   gl_Position =  MVP * skeleton[joint_index] * vec4(vertexPosition_modelspace,1) ;
   UV = vertexUV;
   NORMAL = vertexNORMAL;
};
