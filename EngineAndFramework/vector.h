#ifndef vector_h_
#define vector_h_

#include <stdio.h>
#include "linmath.h"

void vector4_assign(float* r, float *in);

float vector4_len(float *vec);

void vector4_normalize(float *res, float* in);

void vector3_add(float* r, float *a, float *b);

void vector3_sub(float* r, float *a, float *b);

void vector3_mul(float* r, float *a, float *b);

void vector3_div(float* r, float *a, float *b);

void vector3_mul_factor(float* r, float *a, float f);

void vector3_assign(float* r, float *in);

float vector3_len(float *vec);

void vector3_normalize(float *res, float* in);


typedef struct 
{
	float x,y,z;
}VEC3, Vector3D;

typedef struct 
{
	float x,y;
}VEC2,Vector2D;

typedef struct 
{
	float x,y,z,w;
}VEC4,Vector4D;


typedef struct vector4_i_str
{
	int x,y,z,w;
}Vector4DI;

typedef struct vector3_i_str
{
	int x,y,z;
}Vector3DI, VEC3_INT;

typedef struct vector2_i_str
{
	int x,y;
}Vector2DI, VEC2_INT;

typedef struct vec9int_str
{
    int a,b,c, d,e,f, g,h,i;
}Vector9DI, VEC9_INT;

#endif