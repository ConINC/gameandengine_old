#ifndef camera_h_
#define camera_h_

#include "linmath.h"
#include <stdio.h>
#include "vector.h"
#include "input.h"

typedef struct camera_input_struct
{
    char move_forw;
    char move_left;
    char move_right;
    char move_back;
    char turn_left;
    char turn_right;
    char turn_up;
    char turn_down;
    MouseKeyboard_Input *input;
}Camera_Input;

typedef struct camera_struct
{    
    Camera_Input* input;
    
    float movespeed;
    float rotatespeed;
    float *rotation;
    float *position;
    float *forward;
    float *up;

    //These are constantly calculated
    float *forward_direction;
    float *sideway_direction;
    
    float *view;
    float *projection;
    float *vp;
}Camera;

void camera_init(Camera *camera, float movespeed, float rot_speed,
                        float px, float py, float pz,
                        float rx, float ry, float rz,
                        float ux, float uy, float uz,
                        float fov, float aspect, float min, float max);

void camera_set_perspective(Camera *camera, float fov, float aspect, float min,float max);

void camera_set_rotation(Camera *camera, float x,float y,float z);

void camera_set_position(Camera *camera, float x,float y,float z);


//STOPPED WORKING HERE
void camera_update(Camera *camera,MouseKeyboard_Input *input);

#endif