#include "Vector4D.hpp"

Vector4D::Vector4D()
{
}

Vector4D::~Vector4D()
{
    
}

void Vector4D::Vector4D(float x,float y, float z, float w)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->w = w;
}

void Vector4D::setx(float x) 
{
    this->x = x;
}
void Vector4D::sety(float y)
{
    this->y = y;
}
void Vector4D::setz(float z)
{
    this->z = z;
}
void Vector4D::setw(float w)
{
    this->w = w;
}


float Vector4D::length(std::shared_ptr<class Vector4D> a)
{
    float l = a->x * a->x + a->y * a->y + a->z * a->z + a->w * a->w;
    return sqrtf(l);
}

std::shared_ptr<class Vector4D> Vector4D::normalize(std::shared_ptr<class Vector4D> in)
{
    float len = length(in);    
    std::shared_ptr<class Vector3D> ptr(new Vector4D(in->x / len,  in->y / len,in->z / len, in->w/len));
    return ptr;
}

