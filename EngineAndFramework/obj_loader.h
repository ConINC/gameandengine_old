#ifndef obj_loader_h_
#define obj_loader_h_

#include "collection.h"
#include "vector.h"
#include "basic_loader.h"
#include <stdio.h>
#include <stdlib.h>

typedef struct obj_mat
{
    char *colormap_path;
    char *normalmap_path;
    char *specularmap_path;
    char *name;
    float illum;
    float kd1,kd2,kd3;
    float ka1,ka2,ka3;
    float tf1,tf2,tf3;
    float ni;
    float ks1,ks2,ks3;
    
}OBJ_MATERIAL;

typedef struct obj_face_str
{
    char *material;
    _LIST *p_indices;
    _LIST *n_indices;
    _LIST *uv_indices;

}OBJ_Face;

typedef struct obj_group_str
{
    char *groupname;
    _LIST *faces;
}OBJ_Group;

typedef struct obj_data_raw
{
    _LIST *points;
    _LIST *uvs;
    _LIST *normals;
    _LIST *gfm_lines;
}Obj_data_raw;

typedef struct obj_model_str
{
    _LIST *groups;
    _LIST *materials;
    _LIST *p,*uv,*n;
}OBJ_MODEL;

Obj_data_raw *extract_points(_LIST* list);
_LIST* create_group_string(Obj_data_raw* dat);
VEC9_INT *extract_face_indices_from_line(char *l);
//Takes a single face via lines
OBJ_Face *make_face(_LIST *face_Lines);
//take a group via lines
OBJ_Group *make_single_group(_LIST *l);
void print_group(OBJ_Group *group);

_LIST* make_groups(_LIST *group_lines);
void print_list_as_float3(_LIST *l);

void print_list_as_float2(_LIST *l);

void print_obj_model(OBJ_MODEL *model);

_LIST *load_materials(char *fileandpath);

OBJ_MODEL *load_obj_model(char *root, char *obj_mtl_name);

#endif