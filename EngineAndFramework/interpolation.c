#include "interpolation.h"

inline float interpolate_linear(float u, float p1, float p2)
{
    return p1 + (p2 - p1) * u;
}

inline float interpolate_cosine(float u, float p1, float p2)
{   
    //Calculate a slope of with sine

    //Creates a sine of max Interval 2, with wavelength two Pi
    float factor = u * 3.1415f - 3.14115f/2;
    float sinVal = (sinf(factor) +   1) / 2;
    float res = p1 + (p2 - p1) * sinVal;
    return res;
}

inline float interpolate_cubic(float u, float p0, float p1, float p2, float p3)
{
    //Use the first quarter of cosine for smooth transition
    float a0, a1, a2, a3, u2;
    u2 = u * u;
    a0 = p3 - p2 - p0 + p1;
    a1 = p0 - p1 - a0;
    a2 = p2 - p0;
    a3 = p1;

    return (a0 * u2 * u) + (a1*u2) + (a2*u) + a3;
}