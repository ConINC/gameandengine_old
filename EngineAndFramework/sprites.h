#ifndef sprite_h_
#define sprite_h_
#include "material.h"
#include "vector.h"

typedef struct menu_sprite_str
{
    float x,y,w,h;
    Texture_2D *tex;
    int vert_count;
    unsigned int pid, nid, uvid;
    float depth;
    float sprite_move_x, sprite_move_y;
    float shift_x, shift_y;
}MenuSprite;

MenuSprite *create_menuSprite(char *filename, float x, float y, float sx, float sy, float depth);

#endif