#include "gl_misc.h"
#include <glad/glad.h>
#include "shader.h"

///------------------------------------------------------///
///Initialise the graphicsInformation struct for the game to use
///------------------------------------------------------///
GraphicInformation *graphics_viewport_setup(int w, int h, 
                int p_count, float c1,float c2, float c3, 
                float c4, char splitmode)
{
    GraphicInformation *g_info;
    g_info = malloc(sizeof(GraphicInformation));
    g_info->graphics_clearcolor[0] = c1;
    g_info->graphics_clearcolor[1] = c2;
    g_info->graphics_clearcolor[2] = c3;
    g_info->graphics_clearcolor[3] = c4;
    
    g_info->viewport_count = p_count;
    
    //Initialise viewports for the splitscreen mode
    switch(p_count)
    {
        case 1:
        {
            g_info->player1_viewport = (Viewport_Rectangle){ .height = h, .width = w, .ratio = (float)w / (float)h ,.x = 0, .y = 0};
        };
        break;
        
        case 2:
        {
            
            g_info->player1_viewport = (Viewport_Rectangle){ .height = h/2, .width = w, .ratio = (float)w / ((float)h/2) ,.x = 0, .y = h/2};
            g_info->player2_viewport = (Viewport_Rectangle){ .height = h/2, .width = w, .ratio = (float)w / ((float)h/2) ,.x = 0, .y = 0};

        }
        break;
        case 3:
        {
                g_info->player1_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = 0, .y = 0};
                g_info->player2_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = w/2, .y = 0};
                g_info->player3_viewport = (Viewport_Rectangle){ .height = h/2, .width = w, .ratio = (float)w / (float)h , .x = 0, .y = h/2};
        };
        break;        
        case 4:
        {
                g_info->player1_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = 0, .y = 0};
                g_info->player2_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = w/2, .y = 0};
                g_info->player3_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = 0, .y = h/2};
                g_info->player4_viewport = (Viewport_Rectangle){ .height = h/2, .width = w/2, .ratio = (float)w / (float)h , .x = w/2, .y = h/2};
        };
        break;
    }
    
    g_info->window_height = h;
    g_info->window_width = w;
    g_info->window_ratio = (float)w / (float)h;
     
    return g_info;
}

///------------------------------------------------------///
///Returns a GLFWwindow instance 
///------------------------------------------------------///
GLFWwindow *graphics_api_initialise(char *game_name, GraphicInformation *graphic_info, int swapinterval)
{
    if (!glfwInit()) { exit(1); puts("glfwInit failed"); return ((void*)-1); } //game over man!
    
    GLFWwindow *window;    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    window = glfwCreateWindow(graphic_info->window_width, graphic_info->window_height, game_name, (void*)0, (void*)0);
    
    if (!window) { glfwTerminate(); exit(1); }
    glfwMakeContextCurrent(window);
    gladLoadGLLoader( (GLADloadproc) glfwGetProcAddress);
    
    glClearColor(graphic_info->graphics_clearcolor[0],graphic_info->graphics_clearcolor[1],
                graphic_info->graphics_clearcolor[2],graphic_info->graphics_clearcolor[3]);
    glfwSwapInterval(swapinterval);
    glCullFace(GL_BACK);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    return window;   
}

///This function allocates a buffer on the gpu and returns the buffer_id
GLuint create_buffer_float(int size, float *buffer)
{
    GLuint bid;
    glGenBuffers(1, &bid);    
    glBindBuffer(GL_ARRAY_BUFFER, bid);    
    glBufferData(GL_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);
    return bid;
}

///This function allocates a buffer on the gpu and returns the buffer_id
GLuint create_buffer_element_int(int size, int *buffer)
{
    GLuint bid;
    glGenBuffers(1, &bid);    
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bid);    
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, buffer, GL_STATIC_DRAW);

    return bid;
}

