#include "mapfunctions.h"
#include "obj_loader.h"
#include "c_memory.h"

Level *load_level(char * root,char *filename, char *levelname)
{
    int         i,j,k, mat_count, group_count, face_count;
    OBJ_MODEL   *obj;
    Level       *level;
    Vector3D    *points;
    Vector2D    *uvs;
    Vector3D    *normals;
    char *new_pathandfile   = cmalloc(256);
    char *temp_texture_load_string = cmalloc(256);
    strcpy(new_pathandfile, root);
    strcat(new_pathandfile, filename);
    
    printf("Loading map from %s\n", filename);
    level                   = cmalloc(sizeof(Level));
    level->brushes          = list_new();
    level->textures_table   = list_new();
    
    obj                     = load_obj_model(root,filename);
    points                  = (Vector3D*)cmalloc(sizeof(Vector3D) * list_count(obj->p));
    normals                 = (Vector3D*)cmalloc(sizeof(Vector3D) * list_count(obj->n));
    uvs                     = (Vector2D*)cmalloc(sizeof(Vector2D) * list_count(obj->uv));
    
    for(i = 0; i < list_count(obj->p); i++)
    {
        Vector3D *v         = (Vector3D*)list_at(obj->p, i);
        points[i]           = *v;
        printf("P: %f %f %f\n", v->x, v->y, v->z);
    }
    for(i = 0; i < list_count(obj->p); i++)
    {
        Vector2D *v         = (Vector3D*)list_at(obj->uv, i);
        uvs[i]              = *v;
        printf("UV: %f %f \n", v->x, v->y);
    }
    for(i = 0; i < list_count(obj->p); i++)
    {
        Vector3D *v         = (Vector3D*)list_at(obj->n, i);
        normals[i]          = *v;
        printf("N: %f %f %f\n", v->x, v->y, v->z);
    }
    
    int cunt;
    for(i = 0; i < list_count(obj->materials); i++)
    {
        OBJ_MATERIAL *obj_mat_temp   = list_at(obj->materials,i);
            
        
        Texture_2D *newtex;
        strcpy(temp_texture_load_string, root);
        char *fuck =  obj_mat_temp->colormap_path;
        strcat(temp_texture_load_string, obj_mat_temp->colormap_path);
        strcat(temp_texture_load_string, ".tga");
        
        newtex = texture2dtga_fromfile(temp_texture_load_string);
        newtex->name = obj_mat_temp->name;
        newtex->filename = obj_mat_temp->colormap_path;
        newtex->filename_n = obj_mat_temp->normalmap_path;
        newtex->filename_spec = obj_mat_temp->specularmap_path;
        printf("Texture %s added to table\n", newtex->name);
        list_add(level->textures_table, newtex);
    }

    return level;
}


