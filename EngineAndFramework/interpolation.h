#ifndef interpolation_h_
#define interpolation_h_

#include <math.h>

inline float interpolate_linear(float u, float p1, float p2);

inline float interpolate_cosine(float u, float p1, float p2);

inline float interpolate_cubic(float u, float p0, float p1, float p2, float p3);

#endif
