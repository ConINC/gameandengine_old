#include "camera.h"
#include "game.h"
#include "animation.h"
#include "input.h"
#include "shader.h"
#include "gl_misc.h"
#include "model_loader.h"

Game game;

void game_load()
{
    game.menu = malloc(sizeof(GameMenu_1));
    
    game.input = mouse_kb_input_init();
    game.graph_info = graphics_viewport_setup(1280,768,1, 0.3,0.3,0.3,1,'h');
    game.main_window = graphics_api_initialise("Test Game", game.graph_info,1);
    
    void *v = load_animation("content/models/model_static.animation", "anim01", 0);
    game.menu = create_game_menu_1();
    
   
    game.cam = malloc(sizeof(Camera));
    camera_init(game.cam, 1, 1,     10, 0,0,    0,0/57.3,0,  0,1,0,  70 / 57.3,
                (float)game.graph_info->player1_viewport.width / (float)game.graph_info->player1_viewport.height,1,1000);
    
    SHADER_TEXTURE_ONLY *shader_tex = create_texture_only_shader("Simple_textured", 
                                            "content/shader/simple_textured.vs",
                                             "content/shader/simple_textured.fs");
    game.shader = shader_tex;
    
    SHADER_SKINNING_TEST_ONLY *shader_skin = create_skinning_test_only_shader("simple_skinned", 
                                            "content/shader/simple_skinned.vs",
                                            "content/shader/simple_skinned.fs");
    game.shader_skin = shader_skin;
        
    Model *model = malloc(sizeof(Model)); // = load_model_static("content/models/test_static.mesh");
        
    float *points = malloc(sizeof(float) *18);
    float *uvs = malloc(sizeof(float) *12);
    float *normals = malloc(sizeof(float) *18);
    
    //x = left/right                z = up/down
    points[0] = -1;  points[1] = 0;  points[2] = 1;    
    points[3] = 1; points[4] = 0;  points[5]= 1;
    points[6] = -1;  points[7] = 0;  points[8] = -1;    
    points[9] = -1; points[10] = 0; points[11] = -1;
    points[12] = 1; points[13] = 0; points[14] = 1;
    points[15] = 1; points[16] = 0; points[17] = -1;
    
    normals[0] = 0;  normals[1] = 1;  normals[2] = 0;    
    normals[3] = 0; normals[4] = 1;  normals[5]= 0;
    normals[6] = 0;  normals[7] = 1;  normals[8] = 0;    
    normals[9] = 0; normals[10] = 1; normals[11] = 0;
    normals[12] = 0; normals[13] = 1; normals[14] = 0;
    normals[15] = 0; normals[16] = 1; normals[17] = 0;
    
    uvs[0] = 0; uvs[1] = 0;
    uvs[2] = 1; uvs[3] = 0;
    uvs[4] = 0; uvs[5] = 1;
    uvs[6] = 0; uvs[7] = 1;
    uvs[8] = 1; uvs[9] = 0;
    uvs[10] = 1; uvs[11] = 1;
    
    model->p_size = sizeof(float) *18;
    model->uv_size = sizeof(float) *12;
    model->n_size = sizeof(float) *18;
    model->vertex_count = 6;
    
    model->p_id = create_buffer_float(model->p_size, (float*)points);
    model->uv_id = create_buffer_float(model->uv_size,(float*)uvs);
    model->n_id = create_buffer_float(model->n_size, (float*)normals);
    game.model = model;
    
    game.model->texture_ptr = texture2d_fromfile("content/models/xture.bmp");
        
    Model *model_anim;
    //Load neccesairy data for indexed mode
    {
        model_anim = malloc(sizeof(Model)); // = load_model_static("content/models/test_static.mesh");
        
        model_anim->p_size = sizeof(float) * 54;
        model_anim->uv_size = sizeof(float) *36;
        model_anim->n_size = sizeof(float) *54;
        model_anim->w_size = sizeof(float) *18;
            
        points = malloc(model_anim->p_size);
        uvs = malloc(model_anim->uv_size);
        normals = malloc(model_anim->n_size);
        float *joint_index = malloc(model_anim->w_size);
        
        joint_index[0] = 0;
        joint_index[1] = 0;
        joint_index[2] = 0;    
        joint_index[3] = 0;
        joint_index[4] = 0;
        joint_index[5] = 0;
        
        joint_index[6] = 0;
        joint_index[7] = 0;
        joint_index[8] = 1;    
        joint_index[9] = 1;
        joint_index[10] = 0;
        joint_index[11] = 1;
            
        joint_index[12] = 1;
        joint_index[13] = 1;
        joint_index[14] = 2;
        joint_index[15] = 2;
        joint_index[16] = 1;
        joint_index[17] = 2;
        
        points[0] = -1;  points[1] = 0;  points[2] = 0;    
        points[3] = 1; points[4] = 0;  points[5]= 0;
        points[6] = -1;  points[7] = 0;  points[8] = -6;  
      
        points[9] = -1; points[10] = 0; points[11] = -6;
        points[12] = 1; points[13] = 0; points[14] = 0;
        points[15] = 1; points[16] = 0; points[17] = -6;
        
        points[18] = -1;  points[19] = 0;  points[20] = -6;    
        points[21] = 1; points[22] = 0;  points[23]= -6;
        points[24] = -1;  points[25] = 0;  points[26] = -12; //12
        
        points[27] = -1; points[28] = 0; points[29] = -12;//12
        points[30] = 1; points[31] = 0; points[32] = -6;
        points[33] = 1; points[34] = 0; points[35] = -12;//12
        
        points[36] = -1;  points[37] = 0;  points[38] = -12;    
        points[39] = 1; points[40] = 0;  points[41]= -12;
        points[42] = -1;  points[43] = 0;  points[44] = -18; //12
        
        points[45] = -1; points[46] = 0; points[47] = -18;//12
        points[48] = 1; points[49] = 0; points[50] = -12;
        points[51] = 1; points[52] = 0; points[53] = -18;//12
        
        uvs[0] = 0; uvs[1] = 0;
        uvs[2] = 1; uvs[3] = 0;
        uvs[4] = 0; uvs[5] = 1;
        uvs[6] = 0; uvs[7] = 1;
        uvs[8] = 1; uvs[9] = 0;
        uvs[10] = 1; uvs[11] = 1;
        
        uvs[12] = 0; uvs[13] = 0;
        uvs[14] = 1; uvs[15] = 0;
        uvs[16] = 0; uvs[17] = 1;
        uvs[18] = 0; uvs[19] = 1;
        uvs[20] = 1; uvs[21] = 0;
        uvs[22] = 1; uvs[23] = 1;
        
        uvs[24] = 0; uvs[25] = 0;
        uvs[26] = 1; uvs[27] = 0;
        uvs[28] = 0; uvs[29] = 1;
        uvs[30] = 0; uvs[31] = 1;
        uvs[32] = 1; uvs[33] = 0;
        uvs[34] = 1; uvs[35] = 1;
            
        model_anim->vertex_count = 18;
        
        model_anim->p_id = create_buffer_float(model_anim->p_size, (float*)points);
        model_anim->uv_id = create_buffer_float(model_anim->uv_size,(float*)uvs);
        model_anim->n_id = create_buffer_float(model_anim->n_size, (float*)normals);       
        model_anim->w_id = create_buffer_float(model_anim->w_size, (float*)joint_index);       
    }
    
    Texture_2D *tex_ptr = texture2d_fromfile("content/models/xture.bmp");
    
    game.v = 0;
    game.animated = model_anim;
    game.animated->texture_ptr =tex_ptr;
        
    game.m2 = load_model_static("content/models/model_static.mesh_text");
    //game.indexed_model->texture_ptr = game.animated->texture_ptr;
    
    
    load_model_dynamic(
            "content/models/model_static.mesh_text",
            "content/models/model_static.deformer");
    
    Connections *c = load_connections("content/models/model_static.connections");
    _LIST *deformers = load_deformers("content/models/model_static.deformer");
    
    game.m2->texture_ptr =  tex_ptr;
    
    load_obj_model("content/models/","groups");
    //load_level("content/models/", "groups", "Level 1");
    
}

void game_update()
{   
    //gl_func_enable_wireframe(game.input);
    update_mouse_kb_input(game.input);
    //is_mouse_hover(game.menu->)
    
    //menu update has the shift val
    game.v += 0.0002f;
    game.menu->bg_wire->sprite->shift_x = game.v;
    camera_update(game.cam, game.input);
    glfwPollEvents(); 
}

void game_draw()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    Viewport_Rectangle v1,v2,v3,v4;
    int i = 0;
    v1 = game.graph_info->player1_viewport;
    v2 = game.graph_info->player2_viewport;
    glEnable( GL_BLEND );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
    draw_game_menu(game.menu,&game);
    
    for(i = 0; i < game.graph_info->viewport_count; i++)
    {
        //glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
        mat4x4 id,id2;
        mat4x4_identity(id);
        if(i == 2)
            glViewport(game.graph_info->player1_viewport.x,game.graph_info->player1_viewport.y,game.graph_info->player1_viewport.width,game.graph_info->player1_viewport.height);
        if(i == 3)
            glViewport(game.graph_info->player2_viewport.x,game.graph_info->player2_viewport.y,game.graph_info->player2_viewport.width,game.graph_info->player2_viewport.height);
                
        draw_animated_model_test_only(game.input,NULL, 0, game.cam->vp, id,game.animated, game.shader_skin);
        draw_static_model_texture_only(game.cam->vp, id,game.m2, game.shader);
        draw_static_model_texture_only(game.cam->vp, id,game.model, game.shader);    
        
        GLenum err;
        while((err = glGetError()) != GL_NO_ERROR)
        {
            printf("GL threw error:%d\n", err);
        }
    }
    
    
    glfwSwapBuffers(game.main_window);
}