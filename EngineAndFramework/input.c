#include "input.h"
#include <GLFW/glfw3.h>
#include <game.h>


MouseKeyboard_Input *mouse_kb_input_init()
{
    MouseKeyboard_Input *input;
    input = malloc(sizeof(MouseKeyboard_Input));
    
    input->w = 0;
    input->a = 0;
    input->s = 0;
    input->d = 0;
    input->q = 0;
    input->e = 0;
    input->space = 0;
    input->f = 0;
    input->g = 0;
    input->t = 0;
    
    input->lshift = 0;
    input->rshift = 0;
    input->space = 0;
    input->lctrl = 0;
    input->rctrl = 0;
    input->mouse_l = 0;
    input->mouse_r = 0;
    
    input->mouse_y = 0;
    input->mouse_x = 0;
    
    return input;
}

///------------------------------------------------------///
///handle any sort of mouse/keyboard input
///------------------------------------------------------///
void update_mouse_kb_input(MouseKeyboard_Input *input)
{
    //------------------------------------------------------------------------------------//
    //This section is for the mouse only
    //------------------------------------------------------------------------------------//
    double x, y, w_half, h_half;
    
    w_half = (double)game.graph_info->window_width / 2;
    h_half =(double)game.graph_info->window_height / 2;
    
    glfwGetCursorPos(game.main_window, &x,&y);
    
    x = (x - w_half) / w_half;
    y = (y - h_half) / h_half;
    
    input->mouse_x = (float)x;
    input->mouse_y = (float)y;
    
    if(GLFW_PRESS == glfwGetMouseButton(game.main_window, GLFW_MOUSE_BUTTON_LEFT))
        input->mouse_l = 1;
    else
        input->mouse_l = 0;
    
    if(GLFW_PRESS == glfwGetMouseButton(game.main_window, GLFW_MOUSE_BUTTON_RIGHT))
        input->mouse_r = 1;
    else
        input->mouse_r = 0;
    
    //------------------------------------------------------------------------------------//
    
    glfwSetCursorPos(game.main_window, w_half, h_half);
    
    //------------------------------------------------------------------------------------//
    //This section is for the keyboard
    //------------------------------------------------------------------------------------//
    input->w = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_W)) ? 1 : 0;
    input->a = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_A)) ? 1 : 0;
    input->s = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_S)) ? 1 : 0;
    input->d = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_D)) ? 1 : 0;
    input->e = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_E)) ? 1 : 0;
    input->q = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_Q)) ? 1 : 0;
    input->f = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_F)) ? 1 : 0;
    input->g = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_G)) ? 1 : 0;
    input->t = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_T)) ? 1 : 0;
    input->space = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_SPACE)) ? 1 : 0;
    input->lctrl = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_LEFT_CONTROL)) ? 1 : 0;
    input->rctrl = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_RIGHT_CONTROL)) ? 1 : 0;
    input->lshift = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_LEFT_SHIFT)) ? 1 : 0;
    input->rshift = (GLFW_PRESS == glfwGetKey(game.main_window, GLFW_KEY_RIGHT_SHIFT)) ? 1 : 0;
}