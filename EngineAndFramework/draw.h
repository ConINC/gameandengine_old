#ifndef draw_h_
#define draw_h_

#include <stdio.h>
#include <stdlib.h>
#include "model.h"
#include "shader.h"
#include "input.h"
#include "sprites.h"
///------------------------------------------------------///
///Draw a non-animated 3d model with points, uv, normal and single-texture
///------------------------------------------------------///

void draw_static_model_texture_only(float *vp_mat, float *m_mat, Model *model_ptr, SHADER_TEXTURE_ONLY *shader_ptr);
void draw_gui_texture_only(MenuSprite *sprite, float x, float y, float sx, float sy,SHADER_TEXTURE_ONLY *shader_ptr);
void draw_animated_model_test_only(MouseKeyboard_Input* input, Joint_Key **keys, float anim_time, float *vp_mat, float *m_mat, Model *model_ptr, SHADER_SKINNING_TEST_ONLY *shader_ptr);
void draw_static_model_indexed_texture_only(float *vp_mat, float *m_mat, Model *model_ptr, SHADER_TEXTURE_ONLY *shader_ptr);
  
#endif