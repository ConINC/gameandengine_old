#include "vector.h"

#define DEBUG_PRINT 0
#define STRING_MAX 64
#define PTR32_SIZE 4
#define MATRIX_SIZE 64
#define INT_SIZE 4
#define FLOAT_SIZE 4
#define VECTOR3_SIZE 12
#define VECTOR2_SIZE 8
#define VECTOR4_SIZE 16


void vector4_assign(float* r, float *in)
{
    r[0] = in[0]; 
    r[1] = in[1]; 
    r[2] = in[2];
    r[3] = in[3];
}

float vector4_len(float *vec)
{
    float r = vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2] + vec[3]*vec[3];
    return sqrtf(r);
}

void vector4_normalize(float *res, float* in)
{
    float len = vector4_len(in);
    res[0] = in[0] / len;
    res[1] = in[1] / len;
    res[2] = in[2] / len;
    res[3] = in[3] / len;
}

void vector3_add(float* r, float *a, float *b)
{
    r[0] = a[0] + b[0]; 
    r[1] = a[1] + b[1]; 
    r[2] = a[2] + b[2];
}

void vector3_sub(float* r, float *a, float *b)
{
    r[0] = a[0] - b[0];
    r[1] = a[1] - b[1];
    r[2] = a[2] - b[2];
}

void vector3_mul(float* r, float *a, float *b)
{
    r[0] = a[0] * b[0]; 
    r[1] = a[1] * b[1]; 
    r[2] = a[2] * b[2];
}

void vector3_mul_factor(float* r, float *a, float f)
{
    r[0] = a[0] * f; 
    r[1] = a[1] * f; 
    r[2] = a[2] * f;
}

void vector3_assign(float* r, float *in)
{
    r[0] = in[0]; 
    r[1] = in[1]; 
    r[2] = in[2];
}

void vector3_div(float* r, float *a, float *b)
{
    r[0] = a[0] / b[0]; 
    r[1] = a[1] / b[1]; 
    r[2] = a[2] / b[2];
}

float vector3_len(float *vec)
{
    float r = vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2];
    return sqrtf(r);
}

void vector3_normalize(float *res, float* in)
{
    float len = vector3_len(in);
    res[0] = in[0] / len;
    res[1] = in[1] / len;
    res[2] = in[2] / len;
}

void vector3_cross(float* res, float* vec1, float* vec2)
{
    res[0] = ((vec1[1] * vec2[2]) - (vec1[2] * vec2[1]));
    res[1] = ((vec1[2] * vec2[0]) - (vec1[0] * vec2[2]));
    res[2] = ((vec1[0] * vec2[1]) - (vec1[1] * vec2[0]));    
}

void vector3_reflect(float* res, float* vec1, float* vec2)
{
    res[0] = ((vec1[1] * vec2[2]) - (vec1[2] * vec2[1]));
    res[1] = ((vec1[2] * vec2[0]) - (vec1[0] * vec2[2]));
    res[2] = ((vec1[0] * vec2[1]) - (vec1[1] * vec2[0]));    
}

void vector3_rotateX(float *res, float *vec, float f)
{
    res[0] = vec[0];
    res[1] = vec[1] * cosf(f) + vec[2] * sinf(f);
    res[2] = -vec[1] * sinf(f) + vec[2] * cosf(f);
}


void vector3_rotateY(float *res, float *vec, float f)
{
    res[0] = vec[0] * cosf(f) - vec[2] * sinf(f);
    res[1] = vec[1];
    res[2] = vec[1] * sinf(f) + vec[2] * cosf(f);
}


void vector3_rotateZ(float *res, float *vec, float f)
{
    res[0] = vec[0] * cosf(f) +  vec[1] * sinf(f);
    res[1] = -vec[0] * sinf(f) + vec[1] * cosf(f);
    res[2] = vec[2];
}

void vector3_rotate(float *res, float *in, float * rot)
{
    float* m = calloc(16,4);
    float* rotmx = calloc(16,4);
    float* rotmy = calloc(16,4);
    float* rotmz = calloc(16,4);
    float *rotmat = calloc(16,4);
    mat4x4_identity(m);

    mat4x4_rotate_X(rotmx,m,rot[0]);
    mat4x4_rotate_Y(rotmy,m,rot[1]);
    mat4x4_rotate_Z(rotmz,m,rot[2]);
    free(m);
    m = calloc(16,4);
    mat4x4_mul(m, rotmx,rotmy);
    mat4x4_mul(rotmat, m, rotmz);    
    mat4x4_mul_vec4(res, rotmat, in);
    
    free(m);
    free(rotmx);
    free(rotmy);
    free(rotmz);
    free(rotmat);
}

void print_vec3(float* r)
{
    printf("%.3f %.3f %.3f\n", r[0], r[1], r[2]);
}

void print_mat4x4(float* mat_res)
{
    printf("%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n", 
    mat_res[0],mat_res[1],mat_res[2],mat_res[3],
    mat_res[4],mat_res[5],mat_res[6],mat_res[7],
    mat_res[8],mat_res[9],mat_res[10],mat_res[11],
    mat_res[12],mat_res[13],mat_res[14],mat_res[15]);
}