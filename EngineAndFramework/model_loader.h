#ifndef model_loader_h_
#define model_loader_h_

Model *load_model_static(char *filename);

Connections *load_connections(char *filename);

Animation *load_animation(char *filename, char *anim_name, int anim_index);

Model *load_model_dynamic(char *filename, char *deformer_name);

_LIST *load_deformers(char *filename);
#endif