#ifndef CAMERA_HPP
#define CAMERA_HPP



typedef struct camera_struct
{    
    Camera_Input* input;
    
    float movespeed;
    float rotatespeed;
    float *rotation;
    float *position;
    float *forward;
    float *up;

    //These are constantly calculated
    float *forward_direction;
    float *sideway_direction;
    
    float *view;
    float *projection;
    float *vp;
}Camera;

#endif // CAMERA_HPP
