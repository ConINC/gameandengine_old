#ifndef VECTOR3D_HPP
#define VECTOR3D_HPP
#include <iostream>
#include <memory>

class Vector3D
{
protected:
    float x;
    float y;
    float z;
    
public:
    Vector3D();
    ~Vector3D();
    
    void get_values();
    std::shared_ptr<class Vector3D> add(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b);
    std::shared_ptr<class Vector3D> sub(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b);
    std::shared_ptr<class Vector3D> mul(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b);
    std::shared_ptr<class Vector3D> div(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b);
    std::shared_ptr<class Vector3D> div(std::shared_ptr<class Vector3D> v, factor f);
    float length(std::shared_ptr<class Vector3D> a);
    std::shared_ptr<class Vector3D> normalize(std::shared_ptr<class Vector3D> in);
    std::shared_ptr<class Vector3D> cross(std::shared_ptr<class Vector3D> vec1,std::shared_ptr<class Vector3D> vec2);
    float Vector3D::dot(std::shared_ptr<class Vector3D> a, std::shared_ptr<class Vector3D> b);
    std::shared_ptr<class Vector3D> rotate_x(std::shared_ptr<class Vector3D> a, float f);
    std::shared_ptr<class Vector3D> rotate_y(std::shared_ptr<class Vector3D> a, float f);
    std::shared_ptr<class Vector3D> rotate_z(std::shared_ptr<class Vector3D> a, float f);
    std::shared_ptr<class Vector3D> rotate(std::shared_ptr<class Vector3D> in, std::shared_ptr<class Vector3D> rot);
};

#endif // VECTOR3D_HPP
