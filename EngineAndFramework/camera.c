#include "camera.h"

#define FLOAT_SIZE sizeof(float)
#define MATRIX_SIZE sizeof(float)*16
#define VECTOR3_SIZE sizeof(float)*3
#define VECTOR4_SIZE sizeof(float)*4
#define PI 3.1415

void camera_init(Camera *camera, float movespeed, float rot_speed,
                        float px, float py, float pz,
                        float rx, float ry, float rz,
                        float ux, float uy, float uz,
                        float fov, float aspect, float min, float max)
{
    camera->movespeed           = movespeed;
    camera->rotation            = malloc(VECTOR3_SIZE);
    camera->position            = malloc(VECTOR3_SIZE);
    camera->forward             = malloc(VECTOR4_SIZE);
    camera->up                  = malloc(VECTOR3_SIZE);
    camera->rotatespeed         = rot_speed;
    camera->projection          = malloc(MATRIX_SIZE);
    camera->view                = malloc(MATRIX_SIZE);
    camera->sideway_direction   = malloc(VECTOR4_SIZE);
    camera->forward_direction   = malloc(VECTOR4_SIZE);
    camera->position[0]         = px;
    camera->position[1]         = py;
    camera->position[2]         = pz;
    camera->rotation[0]         = rx;
    camera->rotation[1]         = ry;
    camera->rotation[2]         = rz;
    camera->forward[0]          = 1;
    camera->forward[1]          = 0;
    camera->forward[2]          = 0;
    camera->forward[3]          = 0;
    camera->up[0]               = ux;
    camera->up[1]               = uy;
    camera->up[2]               = uz;
    
    camera->vp                  = malloc(MATRIX_SIZE);
    
    mat4x4_perspective(camera->projection, fov, aspect, min, max);
    mat4x4_look_at(camera->view,camera->position, camera->forward, camera->up);
    mat4x4_mul(camera->vp,camera->projection, camera->view);
}

void camera_set_perspective(Camera *camera, float fov, float aspect, float min,float max)
{
    mat4x4_perspective(camera->projection, fov, aspect, min, max);
}

void camera_set_rotation(Camera *camera, float x,float y,float z)
{
    camera->rotation[0] = x;
    camera->rotation[1] = y;
    camera->rotation[2] = z;    
}

void camera_set_position(Camera* camera, float x,float y,float z)
{
    camera->position[0] = x;
    camera->position[1] = y;
    camera->position[2] = z;
}

//STOPPED WORKING HERE
void camera_update(Camera *camera, MouseKeyboard_Input *input)
{
    float m1[16], m2[16], identity[16];
    float rotmatrix_y_side[16], rotmatrix_y_side_temp[16];
    float rotmatrix_x[16], rotmatrix_y[16],rotmatrix_z[16];
    float rotmatrix[16];
    float translate[16];
    float cam_looktoward[3];
    float side_normalized[4];
    mat4x4_identity(&m1);
    mat4x4_identity(&m2);
    mat4x4_identity(&identity);
    
    //check mouse input
    camera->rotation[1] += input->mouse_x * camera->rotatespeed;
    camera->rotation[2] -= input->mouse_y * camera->rotatespeed;
    
    if(camera->rotation[2] > PI/2 - 0.05f)
        camera->rotation[2] = PI/2 -0.05f;
    
    if(camera->rotation[2] < -PI/2 + 0.05f)
        camera->rotation[2] = -PI/2 + 0.05f;
    
    //Rotation matrix now
    mat4x4_rotate_X(&rotmatrix_x, &m1, camera->rotation[0]);
    mat4x4_rotate_Y(&rotmatrix_y, &m1, camera->rotation[1]);
    mat4x4_rotate_Z(&rotmatrix_z, &m1, camera->rotation[2]);
    
    //Create A single rotation matrix from 3 single ones
    mat4x4_mul(&m2, &rotmatrix_x, &rotmatrix_y);
    mat4x4_mul(&rotmatrix, &m2, &rotmatrix_z);
    
    //rotate the base-direction using the rotationmatrix
    mat4x4_mul_vec4(camera->forward_direction, &rotmatrix, camera->forward);
        
    //calculate final view-projection
    vector3_add(cam_looktoward, camera->position, camera->forward_direction);
    mat4x4_look_at(camera->view, camera->position, &cam_looktoward, camera->up);
    mat4x4_mul(camera->vp, camera->projection, camera->view);
    
    //Rotate the side-vector to allow strafing of the camera (left/right movement)
    mat4x4_rotate_Y(&rotmatrix_y_side,&identity,PI/2);
    mat4x4_mul_vec4(camera->sideway_direction, rotmatrix_y_side, camera->forward_direction); 
    camera->sideway_direction[1] = 0;
    camera->sideway_direction[3] = 0;
    vector4_normalize(&side_normalized, camera->sideway_direction);    
    vector4_assign(camera->sideway_direction, &side_normalized);
    
    
    if(input->w != 0)
    {
        camera->position[0] += camera->forward_direction[0];
        camera->position[1] += camera->forward_direction[1];
        camera->position[2] += camera->forward_direction[2];
    }
    if(input->s != 0)
    {
        camera->position[0] -= camera->forward_direction[0];
        camera->position[1] -= camera->forward_direction[1];
        camera->position[2] -= camera->forward_direction[2];
    }
    
    if(input->a != 0)
    {
        camera->position[0] -= camera->sideway_direction[0];
        camera->position[1] -= camera->sideway_direction[1];
        camera->position[2] -= camera->sideway_direction[2];
    }
    if(input->d != 0)
    {
        camera->position[0] += camera->sideway_direction[0];
        camera->position[1] += camera->sideway_direction[1];
        camera->position[2] += camera->sideway_direction[2];
    }
    
}

void camera_set_lookat(Camera* camera)
{
    
}

void camera_move_forward(Camera* camera, float factor)
{
    
}

void camera_move_sideways(Camera* camera, float factor)
{
    
}

void camera_rotate_x(Camera *camera, float factor)
{
    camera->rotation[0] += camera->rotatespeed * factor;
} 

void camera_rotate_y(Camera *camera, float factor)
{
    camera->rotation[1] += camera->rotatespeed * factor;
}

void camera_rotate_z(Camera *camera, float factor)
{
    camera->rotation[2] += camera->rotatespeed * factor;
}
