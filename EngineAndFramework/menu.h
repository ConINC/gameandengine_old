#ifndef menu_h_
#define menu_h_
#include <stdio.h>
#include <stdlib.h>
#include <glad/glad.h>
#include <material.h>
#include "sprites.h"

typedef struct
{ 
    float x, y, w, h;
}MenuRectangle;

typedef struct
{
    MenuRectangle *rect;
    MenuSprite *sprite;
}MenuButton;

typedef struct
{
    MenuButton *bg_title, *bg_wire, *m1_c, *m1_e, *m1_m, *m1_s,*m1_q;
}GameMenu_1;

MenuButton *create_menu_button(char *filename, float x, float y, float sx, float sy, float depth);

int is_mouse_hover(MenuButton *b, float mx,float my, int screen_x, int screen_y);

GameMenu_1 *create_game_menu_1();

#endif