#ifndef basic_loader_h_
#define basic_loader_h_

#include <stdio.h>
#include <stdlib.h>
#include "collection.h"

typedef struct lines_str
{
    int count;
    char **dcharbuf;
}Lines;

char *load_text(const char *filename);

_LIST* split_buffer(char *buf, char *delimeter);

char* load_binary(const char *filename);

#endif




