.PHONY: clean All

All:
	@echo "----------Building project:[ EngineAndFramework - Debug ]----------"
	@cd "EngineAndFramework" && "$(MAKE)" -f  "EngineAndFramework.mk"
clean:
	@echo "----------Cleaning project:[ EngineAndFramework - Debug ]----------"
	@cd "EngineAndFramework" && "$(MAKE)" -f  "EngineAndFramework.mk" clean
